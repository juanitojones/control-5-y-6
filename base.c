#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"


float desvStd(estudiante curso[]){
	return 0.0;
}

float menor(float prom[]){ // funcion  que calcula el promedio menor del curso
    int i,j,aux;
    for(i=0;i<24;i++)
    {
        for(j=0;j<24;j++)
        {
            if(prom[j] > prom[j+1])
            {
                aux = prom[j];
                prom[j] = prom[j+1];
                prom[j+1] = aux;
            }
        }
    }
	return prom[0];
}

float mayor(float prom[]){ //funcion que calcula el promedio mayor del curso
    int i,j,aux;
    for(i=0;i<24;i++)
    {
        for(j=0;j<24;j++)
        {
            if(prom[j]<prom[j+1])
            {
                aux = prom[j];
                prom[j] = prom[j+1];
                prom[j+1] = aux;
            }
        }
    }
	return prom[0];
}
void ingresarnotas(estudiante x[],int num){ //funcion que permite añadir las notas por cada estudiante
    printf("ingrese nota proyecto 1\n");
    scanf("%f",&x[num].asig_1.proy1);
    if(x[num].asig_1.proy1 < 2 || x[num].asig_1.proy1 >7 )
        printf("error,solo se permite notas entre 2 y 7\n");
    else
    {
        printf("ingrese nota proyecto 2\n");
        scanf("%f",&x[num].asig_1.proy2);
        if(x[num].asig_1.proy2 < 2 || x[num].asig_1.proy2 >7)
            printf("error,solo se permite notas entre 2 y 7\n");
        else
        {
            printf("ingrese nota proyecto 3\n");
            scanf("%f",&x[num].asig_1.proy3);
            if(x[num].asig_1.proy3 < 2 || x[num].asig_1.proy3 > 7)
                printf("error,solo se permite notas entre 2 y 7\n");
            else
            {
                printf("ingrese nota control 1 \n");
                scanf("%f",&x[num].asig_1.cont1);
                if(x[num].asig_1.cont1 < 2 || x[num].asig_1.cont1 > 7)
                    printf("error,solo se permite notas entre 2 y 7\n");
                else
                {
                    printf("ingrese nota control 2\n");
                    scanf("%f",&x[num].asig_1.cont2);
                    if(x[num].asig_1.cont2 < 2 || x[num].asig_1.cont2 > 7)
                        printf("error,solo se permite notas entre 2 y 7\n");
                    else
                    {
                        printf("ingrese nota control 3\n");
                        scanf("%f",&x[num].asig_1.cont3);
                        if(x[num].asig_1.cont3 < 2 || x[num].asig_1.cont3 > 7)
                            printf("error,solo se permite notas entre 2 y 7\n");
                        else
                        {
                            printf("ingrese nota control 4\n");
                            scanf("%f",&x[num].asig_1.cont4);
                            if(x[num].asig_1.cont4 < 2 || x[num].asig_1.cont4 > 7)
                                printf("error,solo se permite notas entre 2 y 7\n");
                            else
                            {
                                printf("ingrese nota control 5\n");
                                scanf("%f",&x[num].asig_1.cont5);
                                if(x[num].asig_1.cont5 < 2 || x[num].asig_1.cont5 > 7)
                                   printf("error,solo se permite notas entre 2 y 7\n");
                                else
                                {
                                    printf("ingrese nota control 6\n");
                                    scanf("%f",&x[num].asig_1.cont6);
                                    if(x[num].asig_1.cont6 < 2 || x[num].asig_1.cont6 > 7)
                                        printf("error,solo se permite notas entre 2 y 7\n");
                                    else
                                        printf("notas ingresadas \n");
                                }
                            }
                        }
                    }
                }
            }
        }
    }    
}
void sacarpromedio(estudiante x[],int num){ //funcion que saca el promedio de las notas 
    x[num].prom = (x[num].asig_1.proy1 +x[num].asig_1.proy2 +x[num].asig_1.proy3+ x[num].asig_1.cont1+ x[num].asig_1.cont2+ x[num].asig_1.cont3 +x[num].asig_1.cont4 + x[num].asig_1.cont5 + x[num].asig_1.cont6)/9; 
}

void registroCurso(estudiante curso[]){//funcion que muestra en pantalla las notas del alumno que desea seleccionar
	printf("ingrese el numero del estudiante a quien quiere ingresar sus notas\n"); 
    int i=0;
    for(i =0;i<24;i++)
        printf("estudiante numero %d : %s %s %s \n",i,curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);
    printf("ingrese el numero \n:");
    scanf("%d",&i);
    ingresarnotas(curso,i);
    sacarpromedio(curso,i);
}

void clasificarEstudiantes(char path[],char path2[], estudiante curso[]){//funcion que escribe en un archivo la lista de alumnos aprobados
    FILE *file; // y la lista de alumnos reprobados
    int i;
    if((file =fopen(path,"w"))==NULL){
        printf("error al abrir archivo \n");
        exit(0);
    }
    else
    {
        for(i = 0;i<24;i++)//se escriben en un archivo las notas superiores a 4
        {
            if(curso[i].prom >= 4)
            {
                fprintf(file,"%s\t%s\t%s\tpromedio final :%.1f\n",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM,curso[i].prom);
            }
        }
    }
    printf("\n El registro de estudiantes aprobados se a realizado");
    fclose(file);
    if((file = fopen(path2,"w"))==NULL)
    {
        printf("error al abrir archivo \n");
        exit(0);
    }
    else
    {
        for(i=0;i<24;i++)
        {
            if(curso[i].prom <4)//se escriben las notas inferiores a 4
                fprintf(file,"%s\t%s\t%s\tpromediofinal: %.1f\n",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM,curso[i].prom);
        }
    }
    printf("\nel registro de estudiantes reprobados se a realizado");
    fclose(file);
}


void metricasEstudiantes(estudiante curso[]){//funcion que calcula el promedio general de cada alumno y el promedio de los 24 alumnos
	float promgeneral = 0,listaprom[24],pmenor = 0,pmayor= 0;
    int i = 0;
    for(i = 0;i<24;i++)
    {
        listaprom[i] = curso[i].prom; // matriz listaprom se usara para calcular cual es el mayor o menor promedio del curso
        printf("estudiante %s %s promedio : %.1f\n",curso[i].nombre,curso[i].apellidoP,curso[i].prom);
        promgeneral = (promgeneral + curso[i].prom);
    }
    printf("Promedio general del curso : %.1f\n",(promgeneral/24));
    pmayor = mayor(listaprom);
    pmenor = menor(listaprom);
    printf("promedio mayor : %.1f \n",pmayor);
    printf("promedio menor : %.1f \n",pmenor);
}





void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("aprobados.txt","reprobados.txt", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}

